package toptracker.config;

import java.io.InputStream;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;


public class ConfigLoader {
	
	
	public static ClientConfig loadClientConfig(InputStream is) {
	
		Yaml yaml = new Yaml(new Constructor(ClientConfig.class));
		ClientConfig config = (ClientConfig) yaml.load(is);
		
		return config;
	}
	
	public static ServerConfig loadServerConfig(InputStream is) {
		Yaml yaml = new Yaml(new Constructor(ServerConfig.class));
		ServerConfig config = (ServerConfig) yaml.load(is);
		
		return config;
		
		
	}
		
}
