package toptracker.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;

import toptracker.entity.Scene;


public class UDPClient {

	private DatagramSocket socket;
	private SocketAddress address;
	
	
	public UDPClient(InetAddress address, int port) throws SocketException {
		socket = new DatagramSocket();
		this.address = new InetSocketAddress(address, port);
	}

	public void sendScene(Scene scene) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(scene);
		
		byte[] data = baos.toByteArray();
		DatagramPacket p = new DatagramPacket(data, data.length, address);
		socket.send(p);
	}
	
}
