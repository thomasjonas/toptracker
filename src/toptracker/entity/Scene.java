package toptracker.entity;

import java.io.Serializable;
import java.util.ArrayList;


public class Scene implements Serializable {

	private int nodeId;
	private long timeStamp;
	private ArrayList<Entity> entities;
	
	public Scene(int nodeId) {
		this.nodeId = nodeId;
		entities = new ArrayList<Entity>();
	}
	
	public int getNodeId() {
		return nodeId;
	}
	
	public ArrayList<Entity> getEntities() {
		return entities;
	}
	
	public void setEntities(ArrayList<Entity> entities) {
		this.entities = entities;
	}
	
	public long getTimeStamp() {
		return timeStamp;
	}
	
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}
		
}
