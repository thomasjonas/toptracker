package toptracker.entity;

import java.awt.Point;
import java.io.Serializable;

public class Entity implements Serializable {
	Point screenPosition;
	int userId;
	float area;
	Point[] points;
	long lastUpdate;
				
	public Entity(Point screenPosition, float area, Point[] points) {
		this.screenPosition = screenPosition;
		this.area = area;
		this.points = points;
		this.lastUpdate = System.currentTimeMillis();
	}

	public int getUserId() {
		return userId;
	}
	
	public Point getScreenPosition() {
		return screenPosition;
	}

	public void setScreenPosition(Point screenPosition) {
		this.screenPosition = screenPosition;
	}

	public float getArea() {
		return area;
	}

	public void setArea(float area) {
		this.area = area;
	}

	public Point[] getPoints() {
		return points;
	}

	public void setPoints(Point[] points) {
		this.points = points;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public float match(Entity blob) {
		float x = (float) Math.pow(blob.getScreenPosition().x - this.getScreenPosition().x, 2);
		float y = (float) Math.pow(blob.getScreenPosition().y - this.getScreenPosition().y, 2);
//		double area = Math.pow(blob.getArea() - this.getArea(), 2);
		float distance = (float) Math.sqrt(x + y);
		return (float) Math.exp(-distance * (1/200f));
//		return Math.sqrt(x + y + area);
	}

	public void update(Entity blob) {
		setPoints(blob.getPoints());
		setArea(blob.getArea());
		setScreenPosition(blob.getScreenPosition());
		setLastUpdate(System.currentTimeMillis());
	}
	
	public long getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
