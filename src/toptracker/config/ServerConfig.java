package toptracker.config;

public class ServerConfig {
	int nodeCount;
	int serverPort;
	String endPointIP; // 127.0.0.1
	int endPointPort;

	public int getNodeCount() {
		return nodeCount;
	}
	public void setNodeCount(int nodeCount) {
		this.nodeCount = nodeCount;
	}

	public void setEndPointIP(String endPointIP) {
		this.endPointIP = endPointIP;
	}
	public String getEndPointIP() {
		return endPointIP;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}
	public int getServerPort() {
		return serverPort;
	}
	public int getEndPointPort() {
		return endPointPort;
	}public void setEndPointPort(int endPointPort) {
		this.endPointPort = endPointPort;
	}

}
