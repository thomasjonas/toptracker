package toptracker.config;

public class ClientConfig {

	String serverIP;
	int serverPort;
	int clientID;
	
	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}
	
	public String getServerIP() {
		return serverIP;
	}
	public void setClientID(int clientID) {
		this.clientID = clientID;
	}
	
	public int getClientID() {
		return clientID;
	}
	
	public int getServerPort() {
		return serverPort;
	}
	
	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}
}
