package toptracker.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EntityManager {

	private int node;
	private HashMap<Integer, Entity> entities;
	
	public EntityManager(int node) {
		setNode(node);
		entities = new HashMap<Integer, Entity>();
	}
	
	public HashMap<Integer, Entity> getEntities() {
		return entities;
	}
	
	public void setNode(int node) {
		this.node = node;
	}
	
	public int getNode() {
		return node;
	}
	
	public int numEntities() {
		return entities.size();
	}

	public Entity getEntity(int id) {
		Entity entity = null;
		if(entities.containsKey(id)) {
			entity = entities.get(id);	
		}
		return entity;
	}
	
	public void update() {
		long now = System.currentTimeMillis();
		
		ArrayList<Integer> idsToRemove = new ArrayList<Integer>();
		
		for (Map.Entry<Integer, Entity> entry: entities.entrySet()) {
			int id = entry.getKey();
			Entity e = entry.getValue();
			
			// remove old entity
			if (now - e.getLastUpdate() > 500) {
				idsToRemove.add(id);
			}
		}
		
		for(int id: idsToRemove) {
			entities.remove(id);
			System.out.println("Removed entity "+id);
		}
	}

	public void matchEntity(Entity blob) {
		
		double bestProbability = 0;
		Entity bestMatch = null;
				
		for (Map.Entry<Integer, Entity> entry: entities.entrySet()) {
			int id = entry.getKey();
			Entity e = entry.getValue();
						
			double probability = e.match(blob);
			if (probability > bestProbability) {
				bestProbability = probability;
				bestMatch = e;				
			}
		}
		
		//System.out.println("best: "+ bestProbability);
		if (bestMatch != null && bestProbability > 0.6) {
			bestMatch.update(blob);
		} else {
			addEntity(blob);
		}
		
	}

	private void addEntity(Entity blob) {
		int newId = entities.size()+1;
		blob.setUserId(newId);
		entities.put(newId, blob);
	}

	public ArrayList<Entity> getEntityList() {
		return new ArrayList<Entity>(this.entities.values());
	}

}
