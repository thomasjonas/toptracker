package toptracker.client;

import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import processing.core.*;
import toptracker.config.ClientConfig;
import toptracker.config.ConfigLoader;
import toptracker.entity.Entity;
import toptracker.entity.Scene;
import toptracker.net.UDPClient;

public class TopTracker extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] { TopTracker.class.getName() });
	}

	private UDPClient client;
	private Tracker tracker;
	private ClientConfig config;

	private boolean showLoading = true;
	boolean drawConsole = false;

	@Override
	public void setup() {

		size(640, 480);

		PFont font = createFont("Helvetica", 14);
		this.textFont(font);
		textSize(12);

		InetAddress addr;
		try {
			addr = InetAddress.getLocalHost();

			String hostname = addr.getHostName();

			File file = new File("config/kinectdata-" + hostname + ".yaml");
			if (file.exists()) {
				try {
					loadConfig("config/kinectdata-" + hostname + ".yaml");
				} catch (SocketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				System.out.println("no host specific config found");
				try {
					loadConfig("config/kinectdata-client.yaml");
				} catch (SocketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		frame.setTitle("KinectData Client / " + config.getClientID());
	}

	public void loadConfig(String filename) throws SocketException,
			UnknownHostException {

		try {
			config = ConfigLoader.loadClientConfig(new FileInputStream(
					new File(filename)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(config.getServerIP());
		System.out.println(config.getServerPort());

		client = new UDPClient(InetAddress.getByName(config.getServerIP()),
				config.getServerPort());
	}

	@Override
	public void keyPressed() {
//		if (key == 'r') {
//			tracker.resetScene();
//		}
//
//		if (key == 'c') {
//			drawConsole = !drawConsole;
//		}
	}

	@Override
	public void draw() {

		if (showLoading) {
			background(0);
			fill(255);
			text("waiting for Kinect device ... ", 30, 30);
			showLoading = false;
		} else if (tracker == null) {
			tracker = new Tracker(config.getClientID(), this);
		} else {

			background(0);
			fill(0);

			Scene scene = null;

			try {
				tracker.update();
				PImage sceneImage = tracker.getSceneImage();
				scene = tracker.getScene();
				client.sendScene(scene);
				image(sceneImage, 0, 0);
				
				String[] colors = new String[] { 
			        "FF0000", "00FF00", "0000FF", "FFFF00", "FF00FF", "00FFFF", "000000", 
			        "800000", "008000", "000080", "808000", "800080", "008080", "808080", 
			        "C00000", "00C000", "0000C0", "C0C000", "C000C0", "00C0C0", "C0C0C0", 
			        "400000", "004000", "000040", "404000", "400040", "004040", "404040", 
			        "200000", "002000", "000020", "202000", "200020", "002020", "202020", 
			        "600000", "006000", "000060", "606000", "600060", "006060", "606060", 
			        "A00000", "00A000", "0000A0", "A0A000", "A000A0", "00A0A0", "A0A0A0", 
			        "E00000", "00E000", "0000E0", "E0E000", "E000E0", "00E0E0", "E0E0E0", 
			    };

				for (Entity entity : scene.getEntities()) {
					Point sp = entity.getScreenPosition();
					
					Point[] blobPoints = entity.getPoints();
					
					int fill = (int) Long.parseLong("44"+colors[entity.getUserId()], 16);
					int stroke = (int) Long.parseLong("CC"+colors[entity.getUserId()], 16);
					fill(fill);
					stroke(stroke);
					if (blobPoints.length > 0) {
						beginShape();
						for (int j = 0; j < blobPoints.length; j++) {
							vertex(blobPoints[j].x, blobPoints[j].y);
						}
						endShape(CLOSE);
					}
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			drawInfo();
		}
	}
	
	public void drawInfo() {
		int barCount = 4;
		fill(50);
		noStroke();
		for (int i = 0; i < barCount; ++i) {
			rect(i * width / barCount, height, width / barCount - 1, -20);
		}

		stroke(80);
		for (int y = 0; y < 23; ++y) {
			for (int x = 0; x < 32; ++x) {
				point(x * 20 + 10, y * 20 + 10);
			}
		}
		noStroke();

		int barWidth = width / barCount;
		textSize(12);
		fill(255);
		text("Server IP:" + config.getServerIP(), 5, height - 6);
		text("Server Port:" + config.getServerPort(), barWidth + 5,
				height - 6);
		text("Client ID:" + config.getClientID(), barWidth * 2 + 5,
				height - 6);

		if (frameRate < 20)
			fill(255, 127, 0);
		else
			fill(255);
		text("Framerate: " + (int) frameRate + "fps", barWidth * 3 + 5,
				height - 6);

		stroke(0);
	}
}
