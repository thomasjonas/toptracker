package toptracker.net;

import toptracker.entity.Scene;

public interface ServerListener {
	public void onSceneReceived(Scene scene);
}
