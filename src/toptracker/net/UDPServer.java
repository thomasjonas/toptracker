package toptracker.net;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;

import toptracker.entity.Entity;
import toptracker.entity.Scene;


public class UDPServer extends Thread {

	ArrayList<ServerListener> listeners = new ArrayList<ServerListener>();
	private int port;
	
	public UDPServer(int port) {
		this.port = port;
	}
	
	public void addListener(ServerListener listener) {
		synchronized(listeners) {
			listeners.add(listener);
		}
	}

	public void run() {
		DatagramSocket serverSocket;
		try {
			serverSocket = new DatagramSocket(port);
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}

		byte[] receiveData = new byte[102400];

		while(true) {
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			try {
				serverSocket.receive(receivePacket);
				
				
				InputStream is = new ByteArrayInputStream(receiveData);
				ObjectInputStream ois = new ObjectInputStream(is);

				Object object = ois.readObject();

				if (object instanceof Scene) {
					Scene scene = (Scene) object;
					if (scene != null) {
						for (ServerListener listener: listeners) {
							listener.onSceneReceived(scene);
						}
					}
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
