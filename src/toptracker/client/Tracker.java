package toptracker.client;

import hypermedia.video.Blob;
import hypermedia.video.OpenCV;

import java.awt.Point;
import java.util.HashMap;

import org.OpenNI.Context;
import org.OpenNI.DepthGenerator;
import org.OpenNI.GeneralException;
import org.OpenNI.OutArg;
import org.OpenNI.ScriptNode;
import org.OpenNI.StatusException;

import processing.core.PApplet;
import processing.core.PImage;
import toptracker.entity.Entity;
import toptracker.entity.EntityManager;
import toptracker.entity.Scene;

public class Tracker {

	public static final int IMG_WIDTH = 640;
	public static final int IMG_HEIGHT = 480;
	public static final int MAX_DEPTH = 2100;
	public static final int MIN_DEPTH = 100;
	
	private PApplet applet;
	private OpenCV opencv;
	
	private Scene scene;
	private Context context;
	private DepthGenerator depthGenerator;
	
	private EntityManager entityManager;
	
	private short[] depthMap = new short[640 * 480];
	
	private OutArg<ScriptNode> scriptNode;
	private int clientID;
	
	private PImage sceneImage;

	private void setupTracker() {
		
		try {
			scriptNode = new OutArg<ScriptNode>();
			context = Context.createFromXmlFile("config/openni.xml", scriptNode);
			depthGenerator = DepthGenerator.create(context);
			
			entityManager = new EntityManager(this.clientID);
			
			opencv = new OpenCV(applet);;
			opencv.allocate(IMG_WIDTH, IMG_HEIGHT);
			
			sceneImage = applet.createImage(IMG_WIDTH, IMG_HEIGHT, PApplet.RGB);
			
		} catch (GeneralException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Scene getScene() {
		return scene;
	}

	public void resetScene() {
		
	}

	public Tracker(int clientID, PApplet applet) {
		this.clientID = clientID;
		this.applet = applet;
		setupTracker();
	}

	public PImage getSceneImage() {
		return sceneImage;
	}

	public void filterDepthMap() {
		try {
			depthGenerator.getDepthMap().createShortBuffer().get(depthMap);
			sceneImage.loadPixels();
			sceneImage.pixels = new int[IMG_WIDTH * IMG_HEIGHT];
			for (int x = 0; x < IMG_WIDTH; x++) {
				for (int y = 0; y < IMG_HEIGHT; y++) {
					int pixelNumber = y * IMG_WIDTH + x;
					if (depthMap[pixelNumber] != 0 
							&& depthMap[pixelNumber] < MAX_DEPTH
							&& depthMap[pixelNumber] > MIN_DEPTH) {
						sceneImage.pixels[pixelNumber] = applet.color(80);
					}
				}
			}
			sceneImage.updatePixels();
		} catch (GeneralException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void processBlobs() {
		opencv.copy(sceneImage);
		opencv.blur(OpenCV.BLUR, 13);
		
		// find blobs
		Blob[] blobs = opencv.blobs(2000, IMG_WIDTH * IMG_HEIGHT / 2, 100, false);
		
		// draw blob results
		for (int i = 0; i< blobs.length; i++) {

			Point centroid = blobs[i].centroid;
			Point[] points = blobs[i].points;
			float area = blobs[i].area;
			
			Entity blob = new Entity(centroid, area, points);
			entityManager.matchEntity(blob);
			
		}
	}
	
	public void update() {

		try {
			
			context.waitAndUpdateAll();
			scene = new Scene(clientID);
			entityManager.update();
			
			filterDepthMap();
			processBlobs();
			scene.setEntities(entityManager.getEntityList());
			
		} catch (StatusException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
